package main

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
)

func getEnv(key, defaultValue string) string {
	value, exists := os.LookupEnv(key)
	if exists {
		fmt.Println("Key " + key + " exists with value " + value)
		return value
	}

	fmt.Println("Key " + key + " does not exist! Using default " + defaultValue)
	return defaultValue
}

func loadDotEnv() {
	err := godotenv.Load()

	if err != nil {
		log.Fatal("Error loading .env file")
	}
}
