// TODO: track response times for each endpoint (this may require the use of a histogram or summary)

package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	routeCounter = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "route_count_total",
		Help: "A counter for the unique routes that are used",
	},
		[]string{"code", "route"},
	)
)

func main() {
	loadDotEnv()

	// Environment variable defaults
	port := getEnv("PORT", "8080")
	addr := ":" + port
	runTrafficSimulator, err := strconv.ParseBool(getEnv("RUN_TRAFFIC_SIMULATOR", "true"))

	if err != nil {
		panic(err)
	}

	// Routes
	http.Handle("/metrics", promhttp.Handler())

	http.HandleFunc("/", webServer)
	http.HandleFunc("/hello", helloHandler)

	if runTrafficSimulator {
		go TrafficSimulator()
	}

	fmt.Println("Starting server on port", port)

	if err := http.ListenAndServe(addr, nil); err != nil {
		log.Fatal(err)
	}
}

func webServer(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.Error(w, "404: Path "+r.URL.Path+" not found!", http.StatusNotFound)
		routeCounter.WithLabelValues("404", r.URL.Path).Inc()
		return
	}

	fmt.Fprintf(w, "Welcome to my web server!")
	routeCounter.WithLabelValues("200", "/").Inc()
}

func helloHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/hello" {
		http.Error(w, "404: Path "+r.URL.Path+" not found!", http.StatusNotFound)
		routeCounter.WithLabelValues("404", r.URL.Path).Inc()
		return
	}

	fmt.Fprintf(w, "This is the hello route!")
	routeCounter.WithLabelValues("200", "/hello").Inc()
}
