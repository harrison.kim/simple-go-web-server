# simple-go-web-server

A simple web server using Go for learning purposes

To use the service, run the following command:

```shell
$ go run main.go
```

Prometheus is used to expose metrics on the `/metrics` route

The current custom metric is exposed as `route_count_total`

A subsequent job can be run with the following to simulate traffic against some of the known routes

Enable this job by setting the environment variable `RUN_TRAFFIC_SIMULATOR` to `true`
```shell
export RUN_TRAFFIC_SIMULATOR="true"
```

### Example Output
```
# HELP route_count_total A counter for the unique routes that are used
# TYPE route_count_total counter
route_count_total{code="200",route="/"} 3
route_count_total{code="200",route="/hello"} 1
route_count_total{code="404",route="/asdf"} 1
route_count_total{code="404",route="/helo"} 1
```
