package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"time"
)

func TrafficSimulator() {
	loadDotEnv()

	hostname := getEnv("HOSTNAME", "http://localhost")
	port := getEnv("PORT", "8080")
	url := hostname + ":" + port

	fmt.Println("Running traffic simulator against URL", url)

	var routes [3]string
	routes[0] = "/"
	routes[1] = "/hello"
	routes[2] = "/bad_route"

	for {
		time.Sleep(500 * time.Millisecond)
		route := routes[rand.Intn(len(routes))]
		endpoint := url + route
		fmt.Println("Hitting endpoint", endpoint)
		http.Get(endpoint)
	}
}
